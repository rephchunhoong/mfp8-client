package com.chunhoong.mfp8client;

import java.io.IOException;
import java.util.Base64;

import com.fasterxml.jackson.databind.ObjectMapper;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AuthClient {

    private static OkHttpClient httpClient = new OkHttpClient();

    public String auth() throws IOException {
        // @formatter:off
        RequestBody body = new FormBody.Builder()
                .add("grant_type", "client_credentials")
                .add("scope", "push.application." + Config.APPLICATION_ID)
                .add("scope", "messages.write")
                .build();
        Request request = new Request.Builder().url(Config.AUTH_URL)
                .addHeader("Authorization", getBasicAuthHeader(Config.USERNAME, Config.PASSWORD))
                .post(body)
                .build();
        // @formatter:on
        
        Response response = httpClient.newCall(request).execute();
        String responseText = response.body().string();
        System.out.println("Response: " + responseText);
        return new ObjectMapper().readTree(responseText).get("access_token").asText();
    }

    private static String getBasicAuthHeader(String username, String password) {
        String encodedCredential = Base64.getEncoder().encodeToString((username + ":" + password).getBytes());
        System.out.println("Encoded header: " + encodedCredential);
        return "Basic " + encodedCredential;
    }

}
