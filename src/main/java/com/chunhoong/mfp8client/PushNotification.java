package com.chunhoong.mfp8client;

public class PushNotification {

    private Message message;
    private PushSettings settings;
    private PushTarget target;
    private int notificationType;

    PushNotification(String alert) {
        this.message = new Message(alert);
        this.notificationType = Config.NOTIFICATION_TYPE;
    }

    PushNotification(String alert, String payload) {
        // @formatter:off
        this.message = new Message(alert);
        this.notificationType = Config.NOTIFICATION_TYPE;
        this.settings = new PushSettings()
                .setApns(new APNSSetting(payload))
                .setGcm(new GCMSetting(payload));
        // @formatter:on
    }

    PushNotification(String alert, String payload, String[] deviceIds) {
        // @formatter:off
        this.message = new Message(alert);
        this.notificationType = Config.NOTIFICATION_TYPE;
        this.settings = new PushSettings()
                .setApns(new APNSSetting(payload))
                .setGcm(new GCMSetting(payload));
        this.target = new PushTarget().setDeviceIds(deviceIds);
        // @formatter:on
    }

    public Message getMessage() {
        return message;
    }

    public PushNotification setMessage(Message message) {
        this.message = message;
        return this;
    }

    public PushSettings getSettings() {
        return settings;
    }

    public PushNotification setSettings(PushSettings settings) {
        this.settings = settings;
        return this;
    }

    public PushTarget getTarget() {
        return target;
    }

    public PushNotification setTarget(PushTarget target) {
        this.target = target;
        return this;
    }

    public int getNotificationType() {
        return notificationType;
    }

    public PushNotification setNotificationType(int notificationType) {
        this.notificationType = notificationType;
        return this;
    }

}

class Message {

    private String alert;

    Message(String alert) {
        this.alert = alert;
    }

    public String getAlert() {
        return alert;
    }

    public Message setAlert(String alert) {
        this.alert = alert;
        return this;
    }

}

class PushSettings {

    private APNSSetting apns;
    private GCMSetting gcm;

    public APNSSetting getApns() {
        return apns;
    }

    public PushSettings setApns(APNSSetting apns) {
        this.apns = apns;
        return this;
    }

    public GCMSetting getGcm() {
        return gcm;
    }

    public PushSettings setGcm(GCMSetting gcm) {
        this.gcm = gcm;
        return this;
    }

}

class APNSSetting {

    private String payload;

    public APNSSetting(String payload) {
        this.payload = payload;
    }

    public String getPayload() {
        return payload;
    }

    public APNSSetting setPayload(String payload) {
        this.payload = payload;
        return this;
    }

}

class GCMSetting {

    public GCMSetting(String payload) {
        this.payload = payload;
    }

    private String payload;

    public String getPayload() {
        return payload;
    }

    public GCMSetting setPayload(String payload) {
        this.payload = payload;
        return this;
    }

}

class PushTarget {

    private String[] deviceIds;

    public String[] getDeviceIds() {
        return deviceIds;
    }

    public PushTarget setDeviceIds(String[] deviceIds) {
        this.deviceIds = deviceIds;
        return this;
    }

}
