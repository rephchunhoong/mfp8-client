package com.chunhoong.mfp8client;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PushNotificationClient {

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private static OkHttpClient httpClient = new OkHttpClient();

    public void sendMessage(String alert) {
        try {
            RequestBody body = RequestBody.create(getMessageBody(alert), JSON);

            // @formatter:off
            Request request = new Request.Builder().url(Config.SEND_PUSH_NOTIFICATION_URL)
                    .addHeader("Authorization", Config.BEARER_TOKEN)
                    .post(body)
                    .build();
            // @formatter:on

            Response response = httpClient.newCall(request).execute();
            String responseText = response.body().string();
            System.out.println("Response: " + responseText);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String getMessageBody(String alert) throws IOException {
        PushNotification pushNotification = new PushNotification(alert);
        return new ObjectMapper().writeValueAsString(pushNotification);
    }

}
