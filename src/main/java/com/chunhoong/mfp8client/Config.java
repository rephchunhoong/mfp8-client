package com.chunhoong.mfp8client;

public class Config {

    public static final String SERVER_HOST = "http://localhost:9080/";
    public static final String RUNTIME_NAME = "ribmfp";
    public static final String AUTH_URL = SERVER_HOST + RUNTIME_NAME + "/api/az/v1/token";
    public static final String APPLICATION_ID = "";
    public static final String SEND_PUSH_NOTIFICATION_URL = SERVER_HOST + "imfpush/v1/apps/" + APPLICATION_ID + "/messages";
    public static final int NOTIFICATION_TYPE = 1;
    public static final String USERNAME = "test";
    public static final String PASSWORD = "test";
    public static String BEARER_TOKEN = "";

}
