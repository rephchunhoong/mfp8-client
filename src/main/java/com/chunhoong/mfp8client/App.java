package com.chunhoong.mfp8client;

import java.io.IOException;

public class App {

    public static void main(String[] args) {
        try {
            String bearerToken = new AuthClient().auth();
            System.out.println("Bearer: " + bearerToken);
            Config.BEARER_TOKEN = "Bearer " + bearerToken;
            new PushNotificationClient().sendMessage("Something");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
}
